Arccos EKS with Serverless Pipelines
---

# Purpose
Prove use case for jenkins-x/eks application and component pipeline for Arccos Golf

## Action Items
- Fork https://github.com/jenkins-x/jenkins-x-boot-config repository to new Arccos Bitbucket repository https://bitbucket.org/golfkick/jenkins-x-boot-config
    - **Forking from github is not directly supported by bitbucket**
```bash
         # Workaround
         
         # Make local directory
         mkdir jenkins-x-boot-config

         # Change to new directory
         cd jenkins-x-boot-config

         # Mirror original jenkins-x-boot-config source code from Github
         git clone --mirror https://github.com/jenkins-x/jenkins-x-boot-config.git .
         cd jenkins-x-boot-config

         # Rename remote from `origin` to `upstream`
         git remote rename origin upstream

         # Add your Bitbucket repo (this is where your source code changes will be pushed)
         git remote add origin git@bitbucket.org:golfkick/jenkins-x-boot-config.git

         # Push everything to Bitbucket
         git push --mirror origin

         # To pull updates to jenkins-x-boot-config from Github:
         git pull upstream master

         # To push your code changes to Bitbucket:
         git push origin master
```

- Update jx configuration in new https://bitbucket.org/golfkick/jenkins-x-boot-config repository.
   - Update `jx-requirements.yml` artifact before boot. 
      - Some specific settings
         - Set `clusterName` to `arccos-eks` [Ref](https://github.com/jenkins-x/jenkins-x-boot-config/blob/master/jx-requirements.yml#L6)
         - Set `provider` to `eks` [Ref](https://github.com/jenkins-x/jenkins-x-boot-config/blob/master/jx-requirements.yml#L6)
         - Set `environments` `development`, `qa`, and `production` [Ref](https://github.com/jenkins-x/jenkins-x-boot-config/blob/master/jx-requirements.yml#L9)
         - Set `secrets` to use hashicorp vault [Ref](https://github.com/jenkins-x/jenkins-x-boot-config/blob/master/jx-requirements.yml#L21)
         - Enable `storage` items. [Ref](https://github.com/jenkins-x/jenkins-x-boot-config/blob/master/jx-requirements.yml#L22-L31)
         - Set `webhooks` to `lighthouse` in order to enable bitbucket git provider [Ref](https://github.com/jenkins-x/jenkins-x-boot-config/blob/master/jx-requirements.yml#L35)
   - Create IAM policy required for `eks` cluster creation
      
      IAM Policy: `eksctl_creation`
      
      IAM Policy Arn: `arn:aws:iam::580776469396:policy/eksctl_creation`
```javascript
      {
         "Version": "2012-10-17",
         "Statement": [
            {
                  "Sid": "VisualEditor0",
                  "Effect": "Allow",
                  "Action": [
                     "ec2:AuthorizeSecurityGroupIngress",
                     "ec2:DeleteSubnet",
                     "ec2:AttachInternetGateway",
                     "ec2:DeleteRouteTable",
                     "ec2:AssociateRouteTable",
                     "ec2:DescribeInternetGateways",
                     "autoscaling:DescribeAutoScalingGroups",
                     "ec2:CreateRoute",
                     "ec2:CreateInternetGateway",
                     "ec2:RevokeSecurityGroupEgress",
                     "autoscaling:UpdateAutoScalingGroup",
                     "ec2:DeleteInternetGateway",
                     "ec2:DescribeKeyPairs",
                     "ec2:DescribeRouteTables",
                     "ec2:ImportKeyPair",
                     "ec2:DescribeLaunchTemplates",
                     "ec2:CreateTags",
                     "ec2:CreateRouteTable",
                     "ec2:RunInstances",
                     "cloudformation:*",
                     "ec2:DetachInternetGateway",
                     "ec2:DisassociateRouteTable",
                     "ec2:RevokeSecurityGroupIngress",
                     "ec2:DescribeImageAttribute",
                     "ec2:DeleteNatGateway",
                     "autoscaling:DeleteAutoScalingGroup",
                     "ec2:DeleteVpc",
                     "ec2:CreateSubnet",
                     "ec2:DescribeSubnets",
                     "eks:*",
                     "autoscaling:CreateAutoScalingGroup",
                     "ec2:DescribeAddresses",
                     "ec2:DeleteTags",
                     "ec2:CreateNatGateway",
                     "autoscaling:DescribeLaunchConfigurations",
                     "ec2:CreateVpc",
                     "ec2:DescribeVpcAttribute",
                     "autoscaling:DescribeScalingActivities",
                     "ec2:DescribeAvailabilityZones",
                     "ec2:CreateSecurityGroup",
                     "ec2:ModifyVpcAttribute",
                     "ec2:ReleaseAddress",
                     "ec2:AuthorizeSecurityGroupEgress",
                     "ec2:DeleteLaunchTemplate",
                     "ec2:DescribeTags",
                     "ec2:DeleteRoute",
                     "ec2:DescribeLaunchTemplateVersions",
                     "ec2:DescribeNatGateways",
                     "ec2:AllocateAddress",
                     "ec2:DescribeSecurityGroups",
                     "autoscaling:CreateLaunchConfiguration",
                     "ec2:DescribeImages",
                     "ec2:CreateLaunchTemplate",
                     "autoscaling:DeleteLaunchConfiguration",
                     "ec2:DescribeVpcs",
                     "ec2:DeleteSecurityGroup"
                  ],
                  "Resource": "*"
            },
            {
                  "Sid": "VisualEditor1",
                  "Effect": "Allow",
                  "Action": [
                     "iam:CreateInstanceProfile",
                     "iam:DeleteInstanceProfile",
                     "iam:GetRole",
                     "iam:GetInstanceProfile",
                     "iam:RemoveRoleFromInstanceProfile",
                     "iam:CreateRole",
                     "iam:DeleteRole",
                     "iam:AttachRolePolicy",
                     "iam:PutRolePolicy",
                     "iam:ListInstanceProfiles",
                     "iam:AddRoleToInstanceProfile",
                     "iam:ListInstanceProfilesForRole",
                     "iam:PassRole",
                     "iam:CreateServiceLinkedRole",
                     "iam:DetachRolePolicy",
                     "iam:DeleteRolePolicy",
                     "ec2:DeleteInternetGateway",
                     "iam:DeleteServiceLinkedRole",
                     "iam:GetRolePolicy"
                  ],
                  "Resource": [
                     "arn:aws:ec2:*:*:internet-gateway/*",
                     "arn:aws:iam::580776469396:instance-profile/eksctl-*",
                     "arn:aws:iam::580776469396:role/eksctl-*"
                  ]
            }
         ]
      }
```
   - Create IAM policy for `jx boot` command.
      
     IAM Policy: `jx_boot`
      
     IAM Policy Arn: `arn:aws:iam::580776469396:policy/jx_boot`
```javascript
      {
         "Version": "2012-10-17",
         "Statement": [
            {
                  "Sid": "VisualEditor0",
                  "Effect": "Allow",
                  "Action": [
                     "dynamodb:CreateTable",
                     "s3:GetObject",
                     "cloudformation:ListStacks",
                     "cloudformation:DescribeStackEvents",
                     "dynamodb:DescribeTable",
                     "s3:CreateBucket",
                     "kms:CreateKey",
                     "s3:ListBucket",
                     "s3:PutBucketVersioning",
                     "cloudformation:DescribeStacks"
                  ],
                  "Resource": "*"
            },
            {
                  "Sid": "VisualEditor1",
                  "Effect": "Allow",
                  "Action": [
                     "iam:GetRole",
                     "iam:GetPolicy",
                     "ecr:CreateRepository",
                     "iam:AttachUserPolicy",
                     "iam:CreateRole",
                     "iam:DeleteRole",
                     "iam:AttachRolePolicy",
                     "iam:CreateAccessKey",
                     "iam:CreateOpenIDConnectProvider",
                     "iam:CreatePolicy",
                     "iam:DetachRolePolicy",
                     "cloudformation:CreateStack",
                     "cloudformation:DeleteStack",
                     "ecr:DescribeRepositories",
                     "iam:GetOpenIDConnectProvider"
                  ],
                  "Resource": [
                     "arn:aws:iam::*:policy/CFN*",
                     "arn:aws:iam::*:policy/*jenkins-x-vault*",
                     "arn:aws:iam::*:oidc-provider/*",
                     "arn:aws:iam::*:role/*addon-iamserviceaccoun*",
                     "arn:aws:iam::*:user/*",
                     "arn:aws:ecr:*:*:repository/*",
                     "arn:aws:cloudformation:*:*:stack/JenkinsXPolicies*/*",
                     "arn:aws:cloudformation:*:*:stack/*addon-iamserviceaccount*/*",
                     "arn:aws:cloudformation:*:*:stack/*jenkins-x-vault*/*"
                  ]
            },
            {
                  "Sid": "VisualEditor2",
                  "Effect": "Allow",
                  "Action": [
                     "iam:CreatePolicy",
                     "iam:DetachRolePolicy",
                     "iam:GetPolicy",
                     "iam:CreateRole",
                     "iam:AttachRolePolicy",
                     "iam:GetOpenIDConnectProvider",
                     "iam:CreateOpenIDConnectProvider"
                  ],
                  "Resource": [
                     "arn:aws:iam::*:oidc-provider/*",
                     "arn:aws:iam::*:role/*addon-iamserviceaccoun*",
                     "arn:aws:iam::*:policy/CFN*"
                  ]
            },
            {
                  "Sid": "VisualEditor3",
                  "Effect": "Allow",
                  "Action": "eks:*",
                  "Resource": [
                     "arn:aws:eks:*:*:fargateprofile/*/*/*",
                     "arn:aws:eks:*:*:cluster/*",
                     "arn:aws:eks:*:*:nodegroup/*/*/*"
                  ]
            }
         ]
      }
```

- Provision IAM user for `vault`. `jx boot` will create and attach a policy to this IAM user [Ref](https://jenkins-x.io/docs/getting-started/setup/boot/clouds/amazon/#configuring-vault-for-eks)

- Copy https://bitbucket.org/golfkick/arccos-api/src/master/handicapengine/ to it's own new Arccos Bitbucket repository https://bitbucket.org/golfkick/arccos-handicapengine

- Restore this RDS snapshot database `prod-maindb-reports1-final-snapshot` to Ohio as a Regional Database Location.
    - Use Multi-AZ deployment
    - Use VPC created for eks

- Examine `arccos-handicapengine` repository 
    - Determine which secrets need to be moved to `vault`
 
- Create EKS cluster
    - Commit all changes to https://bitbucket.org/golfkick/jenkins-x-boot-config
    - Run `jx create cluster eks --skip-installation` to create the kubernetes cluster.
    - Run `jx boot` and provide input when prompted.
    - Create `vault` application policy for `development` deployment environment. Capture `vault` RoleId and SecretId

- Import `arccos-handicapengine` repository/project
    - `jx import` [Ref](https://jenkins-x.io/commands/jx_import/)
    - Define pipeline `arccos-handicapengine`
